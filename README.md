# Backoffice Web Client
Frontend para la administración del sistema por parte del administrador de la entidad proveedora
y los supervisores de las entidades.

## Sonarcloud
```text
https://sonarcloud.io/dashboard?id=expropiados_backoffice-web-client
```

## Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).

import stampit from 'stampit';
import { __, any, forEach, includes, keys as ramdaKeys, pluck, uniq, values as ramdaValues, zip, zipObj } from 'ramda';

const EnumStamp = stampit({
  init({ toMap }) {
    const keys = ramdaKeys(toMap);
    const values = ramdaValues(toMap);

    this._validateKeys(keys);
    this._validateValues(values);

    this._armKeys(keys, values);
    this._armValues(keys, values);
  },
  methods: {
    _validateKeys(keys) {
      const illegalKeys = ['_values'];

      if (any(includes(__, illegalKeys), keys)) {
        throw new Error(`Enum keys cannot be any of these: ${illegalKeys}`);
      }
    },
    _validateValues(values) {
      const enumValues = pluck('value', values);
      if (uniq(enumValues).length !== values.length) {
        throw new Error('Enum values must be uniques');
      }
    },

    _armKeys(keys, values) {
      const addKeys = ([k, v]) => { this[k] = v; };
      forEach(addKeys.bind(this), zip(keys, values));
    },
    _armValues(keys, values) {
      const enumValues = pluck('value', values);
      this._values = zipObj(enumValues, keys);
    },

    get(value) {
      const key = this._values[value];
      return this[key];
    },
  },
});

export default EnumStamp;

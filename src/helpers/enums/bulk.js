import EnumStamp from '~/helpers/enums/enum';

const fileTypes = {
  SPECIALIST: { value: 'SPECIALISTS', name: 'Especialistas' },
  PATIENT: { value: 'PATIENTS', name: 'Pacientes' },
};

export const bulkFileTypes = EnumStamp({ toMap: fileTypes });

const states = {
  PENDING: { value: 'PENDING', name: 'Pendiente' },
  IN_PROGRESS: { value: 'PROGRESS', name: 'En Progreso' },
  FINISHED: { value: 'FINISHED', name: 'Finalizado' },
  FAILED: { value: 'FAILED', name: 'Fallido' },
};

export const bulkStates = EnumStamp({ toMap: states });

const bulkFileType = {
  ORIGINAL: { value: 'ORIGINAL', name: 'original' },
  ERROR: { value: 'ERROR', name: 'error' },
};

export const bulkResultFileTypes = EnumStamp({ toMap: bulkFileType });

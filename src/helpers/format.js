import { bulkFileTypes, bulkStates } from '~/helpers/enums/bulk';

export const formatId = (id = 0, size = 4) => {
  return id.toString().padStart(size, '0'.repeat(size));
};

export const formatBulkFileType = (type) => {
  return bulkFileTypes.get(type)?.name.toUpperCase() ?? null;
};

export const formatBulkState = (state) => {
  return bulkStates.get(state)?.name.toUpperCase() ?? '';
};

export const formatDateTimeToLocal = (datetime = null) => {
  if (datetime != null) {
    return new Date(datetime)?.toLocaleString('es-US');
  }
  return new Date().toLocaleString();
};

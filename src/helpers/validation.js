export const DNIRules = [
  v => !!v || 'El campo DNI es obligatorio',
  v => v.length === 8 || 'El campo DNI debe tener 8 dígitos',
  v => /[0-9]$/.test(v) || 'El campo DNI solo puede contener números',
];
export const carnetRules = [
  v => !!v || 'El campo Carnet de Extranjería es obligatorio',
  v => v.length === 9 || 'El campo Carnet de Extranjería debe tener 9 dígitos',
  v => /[0-9]$/.test(v) || 'El campo Carnet de Extranjería solo puede contener números',
];
export const passportRules = [
  v => !!v || 'El campo Pasaporte es obligatorio',
  v => v.length === 9 || 'El campo Pasaporte debe tener 9 dígitos',
  v => /[0-9]$/.test(v) || 'El campo Pasaporte solo puede contener números',
];
export const emailRules = [
  v => !!v || 'El campo Correo es obligatorio',
  v => (v.split('').length <= 0 || /.+@.+\..+/.test(v)) || 'Ingrese correctamente el correo',
];
export const imageFileRules = [
  v => !v || v.size < 2000000 || 'El tamaño de la imagen debe ser menor a 2 MB',
  v => !v || /[^\s]+(.*?)\.(jpg|jpeg|png|gif|JPG|JPEG|PNG|GIF)$/.test(v.name) || 'Formato inválido',
];
export const rucRules = [
  v => !!v || 'El campo RUC es obligatorio',
  v => v.length === 11 || 'El campo RUC debe tener 11 dígitos',
  v => /^[0-9]+$/.test(v) || 'El RUC solo puede contener números',
];
export const companyNameRules = [
  v => !!v || 'El campo Razón Social es obligatorio',
//  v => /^[0-9a-zA-ZÀ-ÿ\u00F1\u00D1 ]+$/.test(v) || 'La razón Social ser válido',
];
export const nameRules = [
  v => !!v || 'El campo Nombre es obligatorio',
  v => /^[a-zA-Z-ZÀ-ÿ\u00F1\u00D1 ]+$/.test(v) || 'El campo Nombre solo puede contener letras',
];
export const fatherLastnameRules = [
  v => !!v || 'El campo Apellido Paterno es obligatorio',
  v => /^[a-zA-Z-ZÀ-ÿ\u00F1\u00D1 ]+$/.test(v) || 'El campo Apellido Paterno solo puede contener letras',
];
export const motherLastnameRules = [
  v => !!v || 'El campo Apellido Materno es obligatorio',
  v => /^[a-zA-Z-ZÀ-ÿ\u00F1\u00D1 ]+$/.test(v) || 'El campo Apellido Materno solo puede contener letras',
];
export const phoneRules = [
  v => !isNaN(v) || 'El campo Número de Celular solo puede contener números',
  v => v === '' || (v && v.length === 9) || 'El campo Número de Celular debe tener 9 dígitos',
];
export const addressRules = [
  v => !!v || 'Debe ingresar la dirección',
];

export const birthDateRules = [
  v => !!v || 'El campo Fecha de Nacimiento es obligatorio',
];

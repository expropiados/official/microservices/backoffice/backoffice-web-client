import jwtDecode from 'jwt-decode';

export const decodeAuthToken = (token) => {
  const body = jwtDecode(token);

  const user = {
    isAdmin: body.admin,
    username: body.sub,
    id: body.us_id,
    uuid: body.us_uid,
    email: body.email,
    imageUrl: body.img,
  };

  const patient = {
    id: body.pa_id,
    uuid: body.pa_uid,
    company: {
      id: body.pa_com_id,
      name: null,
    },
  };

  const specialist = {
    id: body.sp_id,
    uuid: body.sp_uid,
    company: {
      id: null,
      name: null,
    },
  };

  const clientSupervisor = {
    id: body.cs_id,
    uuid: body.cs_uid,
    company: {
      id: body.cs_com_id,
      name: body.cs_com,
    },
  };

  const providerSupervisor = {
    id: body.ps_id,
    company: {
      id: null,
      name: body.ps_com,
    },
  };

  const extraInfo = {
    loggedAt: body.iat,
    expirationAt: body.exp,
  };

  return {
    ...user,
    patient,
    specialist,
    clientSupervisor,
    providerSupervisor,
    extraInfo,
  };
};

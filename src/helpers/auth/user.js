import stampit from 'stampit';

const User = stampit({
  props: {
    username: null,
    id: null,
    uuid: null,
    email: null,
    imageUrl: null,

    patient: {
      id: null,
      uuid: null,
      company: {
        id: null,
      },
    },

    specialist: {
      id: null,
      uuid: null,
      company: {
        id: null,
        name: null,
      },
    },

    clientSupervisor: {
      id: null,
      uuid: null,
      company: {
        id: null,
        name: null,
      },
    },

    providerSupervisor: {
      id: null,
      company: {
        id: null,
        name: null,
      },
    },

    extraInfo: {
      loggedAt: null,
      expirationAt: null,
    },
  },
});

export default User;

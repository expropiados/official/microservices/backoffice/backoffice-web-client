export default {
  admin: [
    {
      title: 'Especialistas',
      icon: 'mdi-doctor',
      to: '/especialistas',
    },
    {
      title: 'Empresas',
      icon: 'mdi-office-building',
      to: '/clientes',
    },
    {
      title: 'Cargas masivas',
      icon: 'mdi-file-upload-outline',
      to: '/cargas-masivas',
    },
    {
      title: 'Reportes',
      icon: 'mdi-chart-bar',
      to: '/reportes-proveedor',
    },
    {
      title: 'Configuración',
      icon: 'mdi-clock-time-seven-outline',
      to: '/entidad-proveedora',
    },
  ],
  clientSupervisor: [
    {
      title: 'Pacientes',
      icon: 'mdi-account-group-outline',
      to: '/pacientes',
    },
    {
      title: 'Cargas masivas',
      icon: 'mdi-file-upload-outline',
      to: '/cargas-masivas',
    },
    {
      title: 'Reportes',
      icon: 'mdi-chart-bar',
      to: '/reportes-clientes',
    },
  ],
  providerSupervisor: [
    {
      title: 'Especialistas',
      icon: 'mdi-doctor',
      to: '/especialistas',
    },
    {
      title: 'Empresas',
      icon: 'mdi-office-building',
      to: '/clientes',
    },
    {
      title: 'Cargas masivas',
      icon: 'mdi-file-upload-outline',
      to: '/cargas-masivas',
    },
    {
      title: 'Reportes',
      icon: 'mdi-chart-bar',
      to: '/reportes-proveedor',
    },
  ],
};

import Vue from 'vue';
import SpecialistService from '@/services/backoffice/specialist-service';

export const state = () => ({
  loading: false,
  specialists: [],
  totalSpecialists: null,
  specialist: null,
  checkedSpecialist: {},
  userInformation: {},
});

export const actions = {
  async getSpecialists({ commit }, params) {
    const service = this.$getBackofficeService(SpecialistService);
    commit('changeLoading', true);
    try {
      const specialists = await service.getAllSpecialists(params);
      commit('storeSpecialists', specialists.data.data);
      commit('storeTotalSpecialists', specialists.data.total);
    } catch (error) {
      commit('catchError', error);
    }
    commit('changeLoading', false);
  },
  async getSpecialist({ commit }, { uuid }) {
    const service = this.$getBackofficeService(SpecialistService);
    commit('changeLoading', true);
    try {
      const specialist = await service.getASpecialist(uuid);
      commit('storeSpecialist', specialist.data);
    } catch (error) {
      commit('catchError', error);
    }
    commit('changeLoading', false);
  },
  async getInformationByDNI({ commit }, dni) {
    const service = this.$getDniService(SpecialistService);
    commit('changeLoading', true);
    try {
      return service.getInformationByDNI(dni);
    } catch (error) {
      commit('catchError', error);
    }
    commit('changeLoading', false);
  },

  async saveSpecialist({ commit }, specialist) {
    const formData = new FormData();
    formData.append('document', specialist.user.document);
    if (specialist.user.typeDocument === 'CARNET EXTRANJERÍA') {
      formData.append('typeDocument', 'CARNET');
    } else {
      formData.append('typeDocument', specialist.user.typeDocument);
    }
    formData.append('name', specialist.user.name);
    formData.append('fatherLastname', specialist.user.fatherLastname);
    formData.append('motherLastname', specialist.user.motherLastname);
    formData.append('email', specialist.user.email);
    if (specialist.phoneNumber != null) { formData.append('phoneNumber', specialist.phoneNumber); }
    if (specialist.user.address != null) { formData.append('address', specialist.user.address); }
    if (specialist.user.birthdate != null) { formData.append('birthdate', specialist.user.birthdate); }
    // if (specialist.specialty != null) formData.append('specialty', specialist.specialty);
    // ONLY in the case that the image is null send it.
    if (specialist.user.image != null) { formData.append('multipartFile', specialist.user.image); }

    const service = this.$getBackofficeService(SpecialistService);
    try {
      await service.saveSpecialist({ specialist: formData });
    } catch (error) {
      commit('catchError', error);
    }
    commit('changeLoading', false);
  },

  checkedInformationSpecialist({ commit }, dni) {
    const service = this.$getBackofficeService(SpecialistService);
    try {
      return service.checkedInformationSpecialist(dni);
    } catch (error) {
      commit('catchError', error);
    }
    commit('changeLoading', false);
  },
  async editSpecialist({ commit }, specialist) {
    const formData = new FormData();
    formData.append('id', specialist.id);
    formData.append('document', specialist.user.document);
    if (specialist.user.typeDocument === 'CARNET EXTRANJERÍA') {
      formData.append('typeDocument', 'CARNET');
    } else {
      formData.append('typeDocument', specialist.user.typeDocument);
    }
    formData.append('name', specialist.user.name);
    formData.append('fatherLastname', specialist.user.fatherLastname);
    formData.append('motherLastname', specialist.user.motherLastname);
    formData.append('email', specialist.email);
    formData.append('phoneNumber', specialist.phoneNumber);
    formData.append('address', specialist.address);
    if (specialist.user.birthdate !== null) { formData.append('birthdate', specialist.user.birthdate); }
    if (specialist.specialty !== null) { formData.append('specialty', specialist.specialty); }
    // ONLY in the case that the image is null send it.
    if (specialist.user.image !== null) { formData.append('multipartFile', specialist.user.image); }
    formData.append('profileImage', specialist.profileImage);
    const service = this.$getBackofficeService(SpecialistService);
    commit('changeLoading', true);
    try {
      await service.editSpecialist({ specialist: formData });
    } catch (error) {
      commit('catchError', error);
    }
    commit('changeLoading', false);
  },
  unlockSpecialist({ commit }, uuid) {
    const service = this.$getBackofficeService(SpecialistService);
    try {
      return service.unlockSpecialist(uuid);
    } catch (error) {
      commit('catchError', error);
    }
    commit('changeLoading', false);
  },
  blockSpecialist({ commit }, uuid) {
    const service = this.$getBackofficeService(SpecialistService);
    try {
      return service.blockSpecialist(uuid);
    } catch (error) {
      commit('catchError', error);
    }
    commit('changeLoading', false);
  },

};
/* example of business logic */
export const getters = {
  getOneSpecialist: _state => _state.specialist,
};
export const mutations = {
  storeSpecialists(_state, specialists) {
    _state.specialists = specialists;
  },
  storeTotalSpecialists(_state, total) {
    _state.totalSpecialists = total;
  },
  storeSpecialist(_state, specialist) {
    _state.specialist = specialist;
  },
  storeNewSpecialist(_state, checkedSpecialist) {
    Vue.set(_state.checkedSpecialist, checkedSpecialist);
  },
  changeLoading(_state, loading) {
    _state.loading = loading;
  },
  catchError(_state, error) {
    _state.error = error;
  },
};

import ProviderCompanyService from '@/services/backoffice/provider-company-service';

export const state = () => ({
  loading: false,
  providerCompany: {},
});

export const actions = {
  async getProviderCompany({ commit }) {
    const service = this.$getBackofficeService(ProviderCompanyService);
    commit('changeLoading', true);
    try {
      const providerCompany = await service.getProviderCompany();
      commit('storeProviderCompany', providerCompany.data);
    } catch (error) {
      commit('catchError', error);
    }
    commit('changeLoading', false);
  },
  async saveProviderCompany({ commit }, providerCompany) {
    const service = this.$getBackofficeService(ProviderCompanyService);
    commit('changeLoading', true);
    try {
      await service.saveProviderCompany(providerCompany);
    } catch (error) {
      commit('catchError', error);
    }
    commit('changeLoading', false);
  },
  updateField({ commit }, { providerCompany, fieldName, updatedValue }) {
    commit('UPDATE_FIELD', { providerCompany, fieldName, updatedValue });
  },
};

export const mutations = {
  storeProviderCompany(_state, providerCompany) {
    _state.providerCompany = providerCompany;
  },
  UPDATE_FIELD(state, { providerCompany, fieldName, updatedValue }) {
    providerCompany[fieldName] = updatedValue;
  },
  changeLoading(_state, loading) {
    _state.loading = loading;
  },
  catchError(_state, error) {
    _state.error = error;
  },
};

import CompanyService from '~/services/backoffice/company-service';

export const state = () => ({
  loading: false,
  clientCompany: null,
  clientCompanies: [],
  totalCompanies: null,
  error: null,
  message: null,
  checkedClientCompany: {},
  newClientCompany: {},
  firstReport: {},
  secondReport: {},
});

export const actions = {
  async getClientCompanies({ commit }, params) {
    const service = this.$getBackofficeService(CompanyService);
    commit('changeLoading', true);
    try {
      const companies = await service.getAllClientCompanies(params);
      console.log(companies);
      commit('storeCompanies', companies.data.data);
      commit('storeTotalCompanies', companies.data.total);
    } catch (error) {
      commit('catchError', error);
    }
    commit('changeLoading', false);
  },
  checkedInformationClientCompany({ commit }, ruc) {
    const service = this.$getBackofficeService(CompanyService);
    try {
      return service.checkedInformationClientCompany(ruc);
    } catch (error) {
      commit('catchError', error);
    }
    commit('changeLoading', false);
  },
  storeNewClientCompany({ commit }, clientCompany) {
    commit('storeNewClientCompany', clientCompany);
  },
  async registerClientCompany({ commit }, clientCompany) {
    const formData = new FormData();
    commit('changeLoading', true);
    formData.append('ruc', clientCompany.ruc);
    formData.append('name', clientCompany.name);
    formData.append('address', clientCompany.address);
    formData.append('phone', clientCompany.phone);
    formData.append('email', clientCompany.email);
    if (clientCompany.multipartFile != null) { formData.append('multipartFile', clientCompany.multipartFile); }
    const service = this.$getBackofficeService(CompanyService);

    try {
      return await service.registerNewClientCompany({ clientCompany: formData });
    } catch (error) {
      commit('catchError', error);
    }
    commit('changeLoading', false);
  },
  async getReportCalificationServiceToClient({ commit }, params) {
    const service = this.$getBackofficeService(CompanyService);
    commit('changeLoading', true);
    try {
      const firstReport = await service.getReportCalificationServiceToClient(params);
      commit('storeFirstReport', firstReport.data);
    } catch (error) {
      commit('catchError', error);
    }
    commit('changeLoading', false);
  },
  async getReportFrequencyAttentionXReason({ commit }, params) {
    const service = this.$getBackofficeService(CompanyService);
    commit('changeLoading', true);
    try {
      const secondReport = await service.getReportFrequencyAttentionXReason(params);
      console.log('PRINT SECONDREPORT');
      console.log(secondReport);
      console.log(secondReport.data);
      commit('storeSecondReport', secondReport.data);
    } catch (error) {
      commit('catchError', error);
    }
    commit('changeLoading', false);
  },
};
/* example of business logic */
export const getters = {
  firstAppointmentOfToday: _state => _state,
};

export const mutations = {
  storeCompanies(_state, companies) {
    _state.clientCompanies = companies;
  },
  storeTotalCompanies(_state, total) {
    _state.totalCompanies = total;
  },
  storeNewClientCompany(_state, clientCompany) {
    _state.newClientCompany = clientCompany;
  },
  storeCompany(_state, company) {
    _state.clientCompanie = company;
  },
  changeLoading(_state, loading) {
    _state.loading = loading;
  },
  catchError(_state, error) {
    _state.error = error;
  },
  storeFirstReport(_state, firstReport) {
    _state.firstReport = firstReport;
  },
  storeSecondReport(_state, secondReport) {
    _state.secondReport = secondReport;
  },
};

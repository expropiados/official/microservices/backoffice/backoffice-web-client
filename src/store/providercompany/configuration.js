import CompanyService from '~/services/backoffice/company-service';

export const state = () => ({
  loading: false,
  checkedClientCompany: {},
  configurationParams: {},
  error: null,
});

export const actions = {
  async updateConfigurationParams({ commit }, configurationParams) {
    const service = this.$getBackofficeService(CompanyService);
    try {
      const id = parseInt(configurationParams.id);
      const params = new Object();
      params.appointmentDuration = configurationParams.appointmentDuration;
      params.appointmentMaxCant = configurationParams.appointmentMaxCant;
      params.appointmentMaxCancelled = configurationParams.appointmentMaxCancelled;
      params.appointmentTimeToCancel = configurationParams.appointmentTimeToCancel;
      await service.updateConfigurationParams(id, params);
    } catch (error) {
      commit('catchError', error);
    }
    commit('changeLoading', false);
  },

  async getConfigurationParams({ commit }, { id }) {
    const service = this.$getBackofficeService(CompanyService);
    try {
      const configuration = await service.getConfigurationParams(id);
      commit('storeConfigurationParams', configuration.data);
    } catch (error) {
      commit('catchError', error);
    }
    commit('changeLoading', false);
  },
};

export const mutations = {
  storeConfigurationParams(_state, configurationParams) {
    _state.configurationParams = configurationParams;
  },
  changeLoading(_state, loading) {
    _state.loading = loading;
  },
  catchError(_state, error) {
    _state.error = error;
  },
};

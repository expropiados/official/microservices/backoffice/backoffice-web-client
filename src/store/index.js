import options from '@/assets/options.js';
import { isNil } from 'ramda';
import jwtDecode from 'jwt-decode';
import { decodeAuthToken } from '~/helpers/auth/jwt';
import { CLIENT_SUPERVISOR, PATIENT, PROVIDER_SUPERVISOR, SPECIALIST } from '~/helpers/auth/roles';

export const actions = {
  nuxtServerInit({ commit }, _) {
    const token = this.$cookiz.get('token');

    if (!token) {
      return;
    }

    const userAuth = decodeAuthToken(token);
    const user = this.$cookiz.get('user') ?? jwtDecode(token);
    const role = this.$cookiz.get('role');

    commit('auth/authenticate', { userAuth });

    if (user && !isNil(role)) {
      console.log('user auth role =>', role, options, CLIENT_SUPERVISOR, PROVIDER_SUPERVISOR);
      commit('auth/setUser', { token, user });
      commit('auth/setRole', role);

      if (role === PATIENT || role === SPECIALIST) {
        commit('auth/setOptions', []);
        return;
      }

      if (role === CLIENT_SUPERVISOR) {
        commit('auth/setOptions', options.clientSupervisor);
        return;
      }

      if (userAuth.isAdmin) {
        commit('auth/setOptions', options.admin);
        return;
      }

      if (role === PROVIDER_SUPERVISOR) {
        commit('auth/setOptions', options.providerSupervisor);
      }
    }
  },
};

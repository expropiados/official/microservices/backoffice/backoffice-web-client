import ClientSupervisorService from '@/services/backoffice/client-supervisor-service';
import SpecialistService from "~/services/backoffice/specialist-service";

export const state = () => ({
  loading: false,
  checkedClientSupervisor: {},
  newClientSupervisor: {},
  supervisors: [],
  totalSupervisors: null,
  error: null,
  clientSupervisor: null
});

export const actions = {
  storeClientSupervisor({ commit }, supervisor) {
    commit('storeNewClientSupervisor', supervisor);
  },
  async saveClientSupervisor({ commit }, { supervisor }) {
    const service = this.$getBackofficeService(ClientSupervisorService);
    try {
      await service.saveClientSupervisor(supervisor);
    } catch (error) {
      commit('catchError', error);
    }
    commit('changeLoading', false);
  },

  async getClientSupervisor({ commit }, { id, params }) {
    const service = this.$getBackofficeService(ClientSupervisorService);
    try {
      const supervisors = await service.getClientSupervisorById(id, params);
      commit('storeSupervisors', supervisors.data.data);
      commit('storeTotalSupervisors', supervisors.data.total);
    } catch (error) {
      commit('catchError', error);
    }
    commit('changeLoading', false);
  },
  async getAClientSupervisor({ commit }, { uuid }) {
    const service = this.$getBackofficeService(ClientSupervisorService);
    commit('changeLoading', true);
    try {
      console.log(uuid);
      const clientSupervisor = await service.getAClientSupervisor(uuid);
      commit('storeClientSupervisor', clientSupervisor.data);
    } catch (error) {
      commit('catchError', error);
    }
    commit('changeLoading', false);
  },
  checkedInformationClientSupervisor({ commit }, dni) {
    const service = this.$getBackofficeService(ClientSupervisorService);
    try {
      return service.checkedInformationClientSupervisor(dni);
    } catch (error) {
      commit('catchError', error);
    }
    commit('changeLoading', false);
  },
  async editClientSupervisor({ commit }, supervisor ) {
    const formData = new FormData();
    console.log(supervisor);
    formData.append('clientSupervisorId', supervisor.clientCompany.id);
    formData.append('clientCompanyId', supervisor.id);
    formData.append('document', supervisor.user.document);
    if (supervisor.user.typeDocument === 'CARNET EXTRANJERÍA') {
      formData.append('typeDocument', 'CARNET')
    }else{
      formData.append('typeDocument', supervisor.user.typeDocument);
    }
    formData.append('name', supervisor.user.name);
    formData.append('fatherLastname', supervisor.user.fatherLastname);
    formData.append('motherLastname', supervisor.user.motherLastname);
    formData.append('email', supervisor.email);
    formData.append('phoneNumber', supervisor.phoneNumber);
    //formData.append('birthdate','1998-06-04');
    if (supervisor.image != null) formData.append('multipartFile', supervisor.image);
    const service = this.$getBackofficeService(ClientSupervisorService);
    try {
      await service.editClientSupervisor({ supervisor: formData });
    } catch (error) {
      commit('catchError', error);
    }
    commit('changeLoading', false);
  },
};
export const getters = {
  getOneClientSupervisor: _state => _state.clientSupervisor,
};
export const mutations = {
  storeClientSupervisor(_state, supervisor){
    _state.clientSupervisor = supervisor;
  },
  storeTotalSupervisors(_state, total) {
    _state.totalSupervisors = total;
  },
  storeSupervisors(_state, supervisors) {
    _state.supervisors = supervisors;
  },
  storeNewClientSupervisor(_state, clientSupervisor) {
    _state.newClientSupervisor = clientSupervisor;
  },
  changeLoading(_state, loading) {
    _state.loading = loading;
  },
  catchError(_state, error) {
    _state.error = error;
  },
};

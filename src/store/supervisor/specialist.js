import SpecialistService from '~/services/backoffice/temp-service';

export const state = () => ({
  loading: false,
  specialists: [],
  error: null,
});

export const actions = {
  async getSpecialists({ commit }) {
    const service = this.$getBackofficeService(SpecialistService);
    commit('changeLoading', true);
    try {
      const specialists = await service.getAllSpecialists();
      commit('storeSpecialists', specialists.data);
    } catch (error) {
      commit('catchError', error);
    }
    commit('changeLoading', false);
  }
};

export const mutations = {
  storeSpecialists(_state, specialists) {
    _state.specialists = specialists;
  },
  changeLoading(_state, loading) {
    _state.loading = loading;
  },
  catchError(_state, error) {
    _state.error = error;
  },
};

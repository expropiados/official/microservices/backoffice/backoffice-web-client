import BulkService from '~/services/backoffice/bulk-service';

export const state = () => ({
  error: null,
  loading: false,
  currentBulk: {},
  allBulks: [],
  bulkState: '',
  currentBulkFile: '',
});

export const actions = {
  async getCurrentBulkLoad({ commit }, { id }) {
    const service = this.$getBackofficeService(BulkService);
    commit('startLoading');
    try {
      const bulk = await service.getBulkLoad({ id });
      commit('storeBulkState', bulk.data.bulkLoad.state);
      commit('storeCurrentBulk', bulk.data);
      commit('stopLoading');
    } catch (error) {
      commit('registerError', error);
    }
  },
  async getAllCurrentBulkLoad({ commit }, params) {
    const service = this.$getBackofficeService(BulkService);
    commit('startLoading', true);
    try {
      const bulks = await service.getAllBulkLoads(params);
      commit('allBulks', bulks.data.data);
    } catch (error) {
      commit('registerError', error);
    }
    commit('stopLoading', false);
  },
  async saveBulkLoad({ commit }, file) {
    const service = this.$getBackofficeService(BulkService);
    commit('startLoading', true);
    try {
      console.log('file', file);
      await service.saveBulkLoad(file);
    } catch (error) {
      commit('registerError', error);
    }
    commit('stopLoading', false);
  },
  async getCurrentBulkFile({ commit }, { id, type }) {
    const service = this.$getBackofficeService(BulkService);
    commit('startLoading');
    try {
      const bulkFile = await service.getBulkLoadFile({ id, type });
      commit('storeCurrentBulkFile', bulkFile.data);
      commit('stopLoading');
    } catch (error) {
      commit('registerError', error);
    }
  },
};

export const mutations = {
  startLoading(_state) {
    _state.loading = true;
  },
  stopLoading(_state) {
    _state.loading = false;
  },
  registerError(_state, error) {
    _state.error = error;
  },
  storeCurrentBulk(_state, bulk) {
    _state.currentBulk = bulk;
  },
  storeBulkState(_state, bulkState) {
    _state.bulkState = bulkState;
  },
  allBulks(_state, allbulks) {
    _state.allBulks = allbulks;
  },
  storeCurrentBulkFile(_state, bulkFile) {
    _state.currentBulkFile = bulkFile;
  },
};

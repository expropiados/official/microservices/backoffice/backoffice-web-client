import PatientService from '~/services/backoffice/patient-service';
import SpecialistService from '~/services/backoffice/specialist-service';

export const state = () => ({
  loading: false,
  patient: null,
  patients: [],
  error: null,
});

export const actions = {
  async getPatients({ commit }) {
    const service = this.$getBackofficeService(PatientService);
    commit('changeLoading', true);
    try {
      const patients = await service.getAllPatients();
      commit('storePatients', patients.data);
    } catch (error) {
      commit('catchError', error);
    }
    commit('changeLoading', false);
  },
  async getPatient({ commit }, { uuid }) {
    const service = this.$getBackofficeService(PatientService);
    commit('changeLoading', true);
    try {
      const patients = await service.getAPatient(uuid);
      commit('storePatient', patients.data);
    } catch (error) {
      commit('catchError', error);
    }
    commit('changeLoading', false);
  },
  async registerPatient({ commit }, { patient }) {
    const service = this.$getBackofficeService(PatientService);
    commit('changeLoading', true);
    try {
      await service.registerNewPatient(patient);
    } catch (error) {
      commit('catchError', error);
    }
    commit('changeLoading', false);
  },
  checkedInformationPatient({ commit }, dni) {
    const service = this.$getBackofficeService(PatientService);
    try {
      return service.checkedInformationPatient(dni);
    } catch (error) {
      commit('catchError', error);
    }
    commit('changeLoading', false);
  },
  async editPatient({ commit }, { patient }) {
    const service = this.$getBackofficeService(PatientService);
    commit('changeLoading', true);
    try {
      await service.editPatient(patient);
    } catch (error) {
      commit('catchError', error);
    }
    commit('changeLoading', false);
  },
};
/* example of business logic */
export const getters = {
  firstAppointmentOfToday: _state => _state,
  getOnePatient: _state => _state.patient,
};

export const mutations = {
  storePatients(_state, patients) {
    _state.patients = patients;
  },
  storePatient(_state, patient) {
    _state.patient = patient;
  },
  changeLoading(_state, loading) {
    _state.loading = loading;
  },
  catchError(_state, error) {
    _state.error = error;
  },
};

import CompanyService from '~/services/backoffice/company-service';

export const state = () => ({
  loading: false,
  checkedClientCompany: {},
  clientCompany: {},
  error: null,
});

export const actions = {
  async updateClientCompany({ commit }, { clientCompany }) {
    const service = this.$getBackofficeService(CompanyService);
    try {
      await service.updateAClientCompany(clientCompany);
    } catch (error) {
      commit('catchError', error);
    }
    commit('changeLoading', false);
  },

  async getClientCompany({ commit }, { id }) {
    const service = this.$getBackofficeService(CompanyService);
    try {
      const supervisors = await service.getClientCompanyById(id);
      commit('storeClientCompany', supervisors.data);
    } catch (error) {
      commit('catchError', error);
    }
    commit('changeLoading', false);
  },
};

export const mutations = {
  storeClientCompany(_state, clientCompany) {
    _state.clientCompany = clientCompany;
  },
  changeLoading(_state, loading) {
    _state.loading = loading;
  },
  catchError(_state, error) {
    _state.error = error;
  },
};

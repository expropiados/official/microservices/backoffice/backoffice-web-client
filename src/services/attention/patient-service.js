import stampit from 'stampit';
import ServiceStamp from '~/services/service';

export const PatientServiceStamp = stampit.methods({
  getPatient({ id }) {
    return this.fetchApi.get(`attention/patients/${id}`);
  },
});

const CompanyService = stampit.compose(ServiceStamp, PatientServiceStamp);
export default CompanyService;

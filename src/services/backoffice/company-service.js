import stampit from 'stampit';
import ServiceStamp from '~/services/service';

export const CompanyServiceStamp = stampit.methods({
  getAllClientCompanies(params) {
    return this.fetchApi.get('v2/companies', { params });
  },
  checkedInformationClientCompany(ruc) {
    return this.fetchApi.get('companies/checking', { params: { ruc } });
  },
  registerNewClientCompany({ clientCompany }) {
    return this.fetchApi.post('companies', clientCompany, { headers: { 'Content-Type': 'multipart/form-data' } });
  },
  updateAClientCompany(clientCompany) {
    return this.fetchApi.post('companies/edit', clientCompany, { headers: { 'Content-Type': 'multipart/form-data' } });
  },
  getClientCompanyById(id) {
    return this.fetchApi.get(`companies/supervisor?clientCompanyId=${id}`);
  },
  getConfigurationParams(id) {
    return this.fetchApi.get(`companies/${id}/parameters`);
  },
  updateConfigurationParams(id, params) {
    return this.fetchApi.post(`v2/companies/${id}/parameters`, params);
  },
  getReportCalificationServiceToClient(params) {
    return this.fetchApi.get('reports/pre-poll/freq_monthly', { params });
  },
  getReportFrequencyAttentionXReason(params) {
    return this.fetchApi.get('reports/appointment/prom_score', { params });
  },
});

const CompanyService = stampit.compose(ServiceStamp, CompanyServiceStamp);
export default CompanyService;

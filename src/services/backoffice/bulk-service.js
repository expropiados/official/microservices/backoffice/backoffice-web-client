import stampit from 'stampit';
import ServiceStamp from '~/services/service';

export const BulkServiceStamp = stampit.methods({
  getBulkLoad({ id }) {
    return this.fetchApi.get(`bulk-loads/${id}`);
  },
  getAllBulkLoads(params) {
    return this.fetchApi.get('bulk-loads', params);
  },
  saveBulkLoad(file) {
    const form = new FormData();
    form.append('file', file);
    const headers = { 'Content-Type': 'multipart/form-data' };
    return this.fetchApi.post('bulk-loads', form, { headers });
  },
  getBulkLoadFile({ id, type }) {
    return this.fetchApi.get(`bulk-loads/${id}/files?type=${type}`);
  },
});

const BulkService = stampit.compose(ServiceStamp, BulkServiceStamp);
export default BulkService;

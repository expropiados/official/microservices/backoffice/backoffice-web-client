import stampit from 'stampit';
import ServiceStamp from '~/services/service';

export const ProviderCompanyServiceStamp = stampit.methods({
  getProviderCompany() {
    return this.fetchApi.get('provider-companies');
  },
  saveProviderCompany(providerCompany) {
    return this.fetchApi.post('provider-companies', providerCompany);
  },
});

const ProviderCompanyService = stampit.compose(ServiceStamp, ProviderCompanyServiceStamp);
export default ProviderCompanyService;

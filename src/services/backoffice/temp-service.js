import stampit from 'stampit';
import ServiceStamp from '~/services/service';

export const SpecialistServiceStamp = stampit.methods({
  getAllSpecialists() {
    return this.fetchApi.get('specialists');
  }
});

const SpecialistService = stampit.compose(ServiceStamp, SpecialistServiceStamp);
export default SpecialistService;

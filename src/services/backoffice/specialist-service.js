import stampit from 'stampit';
import ServiceStamp from '~/services/service';

export const SpecialistServiceStamp = stampit.methods({

  getAllSpecialists(params) {
    return this.fetchApi.get('specialists', { params });
  },

  saveSpecialist({ specialist }) {
    return this.fetchApi.post(`specialists`, specialist, { headers: { 'Content-Type': 'multipart/form-data' } });
  },
  editSpecialist({ specialist }) {
    console.log(specialist);
    return this.fetchApi.post('specialists/edit', specialist, { headers: { 'Content-Type': 'multipart/form-data' } });
  },
  checkedInformationSpecialist(document) {
    console.log(this.fetchApi);
    return this.fetchApi.get(`specialists/checking`, { params: { document } });
  },
  getASpecialist(uuid) {
    return this.fetchApi.get(`specialists/?specialistUUID=${uuid}`);
  },
  unlockSpecialist(uuid) {
    return this.fetchApi.post(`specialists/unblock?specialistUUID=${uuid}`);
  },

  blockSpecialist(uuid) {
    return this.fetchApi.post(`specialists/block?specialistUUID=${uuid}`);
  },
});

const SpecialistService = stampit.compose(ServiceStamp, SpecialistServiceStamp);
export default SpecialistService;

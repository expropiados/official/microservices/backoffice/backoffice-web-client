import stampit from 'stampit';
import ServiceStamp from '~/services/service';

export const PatientServiceStamp = stampit.methods({
  getAllPatients() {
    return this.fetchApi.get('patients');
  },
  registerNewPatient(patient) {
    console.log(patient);
    return this.fetchApi.post('patients', patient, { headers: { 'Content-Type': 'multipart/form-data' } });
  },
  editPatient(patient) {
    return this.fetchApi.post('patients/edit', patient, { headers: { 'Content-Type': 'multipart/form-data' } });
  },
  checkedInformationPatient(dni) {
    // console.log(dni);
    return this.fetchApi.get('patients/checking', { params: { dni } });
  },
  getAPatient(uuid) {
    return this.fetchApi.get(`patients/?patientUUID=${uuid}`);
  },
});

const PatientService = stampit.compose(ServiceStamp, PatientServiceStamp);
export default PatientService;

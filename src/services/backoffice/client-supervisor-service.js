import stampit from 'stampit';
import ServiceStamp from '~/services/service';

export const ClientSupervisorServiceStamp = stampit.methods({
  saveClientSupervisor(supervisor) {
    return this.fetchApi.post('companies/supervisor', supervisor, { headers: { 'Content-Type': 'multipart/form-data' } });
  },
  checkedInformationClientSupervisor(document) {
    return this.fetchApi.get('companies/supervisor/checking', { params: { document } });
  },
  getClientSupervisorById(id, params) {
    return this.fetchApi.get(`v2/companies/${id}/supervisors`, { params });
  },
  getAClientSupervisor(uuid) {
    return this.fetchApi.get(`companies/supervisor/?clientSupervisorUUID=${uuid}`);
  },
  editClientSupervisor({supervisor}){
    return this.fetchApi.post('companies/supervisor/edit', supervisor, { headers: { 'Content-Type': 'multipart/form-data' } });
  }
});

const ClientSupervisorService = stampit.compose(ServiceStamp, ClientSupervisorServiceStamp);
export default ClientSupervisorService;

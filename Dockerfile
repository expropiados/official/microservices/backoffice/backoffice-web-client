FROM node:14.15-alpine as base
WORKDIR /app

## Download dependencies to build
FROM base as dependencies
COPY package.json package-lock.json ./
RUN npm install --frozen-lockfile

## Build project with dependencies
FROM base AS builder
ENV NODE_ENV=production
WORKDIR /app
COPY . .
COPY --from=dependencies /app/node_modules /app/node_modules
RUN npm run build

## Release project app
FROM base AS release

EXPOSE 3000
ENV NODE_ENV=production
ENV HOST=0.0.0.0
ENV PORT=3000

WORKDIR /app
COPY --from=builder /app/nuxt.config.js ./
COPY --from=builder /app/.nuxt ./.nuxt
COPY --from=builder /app/node_modules ./node_modules

CMD ["node_modules/.bin/nuxt", "start"]
